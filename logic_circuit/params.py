"""Defines logic input variables that can be used in circuits.
"""

from logic_circuit.core import LogicVariableBase


class A(LogicVariableBase):
    """Logical input variable called A."""


class B(LogicVariableBase):
    """Logical input variable called B."""


class C(LogicVariableBase):
    """Logical input variable called C."""


class D(LogicVariableBase):
    """Logical input variable called D."""


class E(LogicVariableBase):
    """Logical input variable called E."""


class F(LogicVariableBase):
    """Logical input variable called F."""


class G(LogicVariableBase):
    """Logical input variable called G."""


class H(LogicVariableBase):
    """Logical input variable called H."""


class I(LogicVariableBase):
    """Logical input variable called I."""


class J(LogicVariableBase):
    """Logical input variable called J."""


class K(LogicVariableBase):
    """Logical input variable called K."""


class L(LogicVariableBase):
    """Logical input variable called L."""


class M(LogicVariableBase):
    """Logical input variable called M."""


class N(LogicVariableBase):
    """Logical input variable called N."""


class O(LogicVariableBase):
    """Logical input variable called O."""


class P(LogicVariableBase):
    """Logical input variable called P."""


class Q(LogicVariableBase):
    """Logical input variable called Q."""


class R(LogicVariableBase):
    """Logical input variable called R."""


class S(LogicVariableBase):
    """Logical input variable called S."""


class T(LogicVariableBase):
    """Logical input variable called T."""


class U(LogicVariableBase):
    """Logical input variable called U."""


class V(LogicVariableBase):
    """Logical input variable called V."""


class W(LogicVariableBase):
    """Logical input variable called W."""


class X(LogicVariableBase):
    """Logical input variable called X."""


class Y(LogicVariableBase):
    """Logical input variable called Y."""


class Z(LogicVariableBase):
    """Logical input variable called Z."""
