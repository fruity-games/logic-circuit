from logic_circuit.params import *

test = lambda c: print("{} = {}".format(c, c.evaluate()))

A.state = 1
B.state = 0
C.state = 1
test(A & (B | ~C))
